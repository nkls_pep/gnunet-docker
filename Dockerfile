FROM debian:buster-slim

RUN apt update && apt upgrade -y && \
apt install -y git libtool autoconf autopoint \
build-essential libgcrypt-dev libidn11-dev zlib1g-dev \
libunistring-dev libglpk-dev miniupnpc libextractor-dev \
libjansson-dev libcurl4-gnutls-dev gnutls-bin libsqlite3-dev \
openssl libnss3-tools libopus-dev libpulse-dev libogg-dev \ 
libargon2-dev libsodium-dev libgnutls28-dev htop wget \
python3-pip nano nmap && pip3 install requests

COPY pygnunetrest pygnunetrest
COPY testdata testdata

WORKDIR /
RUN echo "building libmicrohttpd" && \
/usr/bin/wget https://ftp.gnu.org/gnu/libmicrohttpd/libmicrohttpd-0.9.71.tar.gz && \
/bin/tar -xf /libmicrohttpd-0.9.71.tar.gz && \
/bin/rm /libmicrohttpd-0.9.71.tar.gz

WORKDIR /libmicrohttpd-0.9.71
RUN autoreconf -fi && \
./configure --disable-doc --prefix=/opt/libmicrohttpd && \
make -j$(nproc || echo -n 1) && \
make install

WORKDIR /
RUN echo "building gnunet"
RUN /usr/bin/wget http://ftpmirror.gnu.org/gnunet/gnunet-0.13.3.tar.gz && \
/bin/tar -xf /gnunet-0.13.3.tar.gz && \
/bin/rm gnunet-0.13.3.tar.gz

WORKDIR /gnunet-0.13.3
ENV GNUNET_PREFIX=/usr/local
ENV CFLAGS="-g -Wall -O0"
RUN ./configure --prefix=$GNUNET_PREFIX --disable-documentation --enable-logging=verbose --with-microhttpd=/opt/libmicrohttpd && \
/usr/sbin/addgroup gnunet && \
/usr/sbin/addgroup gnunetdns && \
/usr/sbin/adduser --system --home /var/lib/gnunet gnunet && \
/usr/sbin/usermod -aG gnunet root && \
make -j$(nproc || echo -n 1) && \
make install

WORKDIR /
RUN mkdir /gnunet-config
RUN touch /gnunet-config/gnunet.conf
RUN mkdir -p /logs
RUN touch /logs/arm.log


ENV LD_LIBRARY_PATH=/usr/local/lib

COPY docker-entrypoint.sh /opt

ENV PATH="/usr/local/share/bin:$PATH"

ENTRYPOINT ["/opt/docker-entrypoint.sh"]
